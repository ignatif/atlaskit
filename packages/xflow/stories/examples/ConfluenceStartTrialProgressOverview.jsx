import React from 'react';
import { ConfluenceStartTrialProgress } from '@atlaskit/xflow';

export default (
  <ConfluenceStartTrialProgress />
);
