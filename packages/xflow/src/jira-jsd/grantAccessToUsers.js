import grantAccessToUsers from '../common/grantAccessToUsers';

export default grantAccessToUsers('jira-servicedesk-users', 'Jira Service Desk', 'Grants access to Jira Service Desk');
