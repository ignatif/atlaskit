import { getAlreadyRequestedFlag } from '../common/alreadyRequestedFlag';

export default () => getAlreadyRequestedFlag('confluence.ondemand');
