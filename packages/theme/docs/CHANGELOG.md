# @atlaskit/tag-group

## 2.2.0 (2017-09-27)

* feature; export "layers" from theme ([15aebe6](https://bitbucket.org/atlassian/atlaskit/commits/15aebe6))

## 2.1.0 (2017-09-13)

* feature; [@atlaskit](https://github.com/atlaskit)/theme now has a named getTheme() function export ([b727679](https://bitbucket.org/atlassian/atlaskit/commits/b727679))

## 2.0.1 (2017-08-11)

* bug fix; Add placeholder color to theme ([ba023fb](https://bitbucket.org/atlassian/atlaskit/commits/ba023fb))

## 2.0.0

Initial Release
