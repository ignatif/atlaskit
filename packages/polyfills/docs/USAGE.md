# @atlaskit/polyfills

This package contains polyfills for native methods which are unsupported by some of the browsers that we target.

## Try it out

Interact with a [live demo of the @NAME@ component](https://aui-cdn.atlassian.com/atlaskit/stories/@NAME@/@VERSION@/).

## Installation

```sh
npm install @NAME@
```
