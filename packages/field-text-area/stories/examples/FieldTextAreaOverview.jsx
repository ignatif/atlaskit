import React from 'react';
import FieldTextArea from '@atlaskit/field-text-area';

export default (
  <FieldTextArea
    placeholder="This is a placeholder"
    label="An overview of a field text"
  />
);
