import FieldTextArea from './components/FieldTextArea';
import FieldTextAreaStateless from './components/FieldTextAreaStateless';

export default FieldTextArea;
export { FieldTextAreaStateless };
