const path = require('path');

module.exports = [
  { name: 'Text Field Area', src: path.join(__dirname, '../src/components/FieldTextArea.jsx') },
  { name: 'FieldTextAreaStateless', src: path.join(__dirname, '../src/components/FieldTextAreaStateless.jsx') },
];
