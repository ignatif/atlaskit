# ButtonGroup

> This package has been deprecated. Please import ButtonGroup from [@atlaskit/button](https://www.npmjs.com/package/@atlaskit/button) instead.

A button group provides a visual grouping for related Button elements. You can use this component whenever you have multiple buttons with related actions, such as as the pull requests Approve and Decline buttons.

![Example button group](https://bytebucket.org/atlassian/atlaskit/raw/@BITBUCKET_COMMIT@/packages/button-group/docs/button_group.png)

## Try it out

Detailed docs and example usage can be found [here](https://aui-cdn.atlassian.com/atlaskit/stories/@NAME@/@VERSION@/).

## Setup and install

```
npm install @NAME@
```
