# @atlaskit/spotlight

## 1.1.0 (2017-10-06)

* feature; spotlight allows to specific width ([ba77127](https://bitbucket.org/atlassian/atlaskit/commits/ba77127))
## 1.0.0-beta (2017-09-19)

* feature; initial release
