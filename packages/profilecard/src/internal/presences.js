const presences = {
  none: '',
  available: 'Available',
  busy: 'Busy',
  unavailable: 'Away',
};

export default presences;
